﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMove : MonoBehaviour {

    public float maxSpeed = 5.0f;
    public float acceleration = 3.5f;
    private float speed = 0.0f;
    public float brake = 5.0f;
    public float turnSpeed=30.0f;

    void Start () { 
    }
    
    void Update()
    {
        // the horizontal axis controls the turn
        float turn = Input.GetAxis("Horizontal");

        // turn the car
        transform.Rotate(0, 0, turn * turnSpeed * Time.deltaTime * speed);

        // the vertical axis controls acceleration fwd/back
        float forwards = Input.GetAxis("Vertical");

        if (forwards > 0)
        {
            // accelerate forwards
            speed = speed + acceleration * Time.deltaTime;
        }
        else if (forwards < 0)
        {
            // accelerate backwards
            speed = speed - acceleration * Time.deltaTime;
        }
        else
        {
            // braking
            if (speed > 0)
            {
                speed = speed - brake * Time.deltaTime;
                speed = Mathf.Clamp(speed, 0, maxSpeed);
            }
            else if (speed < 0)
            {
                speed = speed + brake * Time.deltaTime;
                speed = Mathf.Clamp(speed, -maxSpeed, 0);
            }
            else
                speed = 0;

        }
        // clamp the speed
        speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

        // compute a vector in the up direction of length speed
        Vector2 velocity = Vector2.up * speed;

        // move the object
        transform.Translate(velocity * Time.deltaTime);
    }
}
