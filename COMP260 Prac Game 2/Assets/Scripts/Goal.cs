﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Goal : MonoBehaviour {

    public AudioClip scoreClip;
    private AudioSource audio;
    public int player = 0;

    void Start () {
        audio = GetComponent<AudioSource>();
    }
	
	void Update () {
		
	}

    void OnTriggerEnter(Collider collider)
    {
        audio.PlayOneShot(scoreClip);
        Scorekeeper.Instance.OnScoreGoal(player);
        PuckControl puck = collider.gameObject.GetComponent<PuckControl>();
        puck.ResetPosition();
    }
}
