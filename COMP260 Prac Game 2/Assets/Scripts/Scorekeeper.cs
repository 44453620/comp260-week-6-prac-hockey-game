﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scorekeeper : MonoBehaviour {

    static private Scorekeeper instance;
    static public Scorekeeper Instance
    {
        get { return instance; }
    }    public int pointsPerGoal = 1;
    private int[] score = new int[2];    public Text[] scoreText;
    public Text winText;

    void Start()
    {
        if (instance == null)
            instance = this;
        else
            Debug.LogError("More than one Scorekeeper exists in the scene.");
        // reset the scores to zero
        for (int i = 0; i < score.Length; i++)
        {
            score[i] = 0;
            scoreText[i].text = "0";
        }
    }
    public void OnScoreGoal(int player)
    {
        score[player] += pointsPerGoal;
        scoreText[player].text = score[player].ToString();
        if (score[player] == 2)
        {
            winText.text = "player "+ player + " wins";
            Time.timeScale = 0;
        }
    }
  }