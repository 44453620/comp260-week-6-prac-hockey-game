﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PaddleKeyboard : MonoBehaviour {

    public float speed = 10f;
    Rigidbody rb;
	
	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	void Update () {
        float hor = Input.GetAxis("Horizontal");
        float ver = Input.GetAxis("Vertical");
        Vector3 mov = new Vector3(hor * speed, 0, ver * speed);
        rb.velocity = mov;
    }
}
