﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goalie : MonoBehaviour {

    public Rigidbody rb;
    public Rigidbody puck;
    public Transform puckPos;
    public float speed = 10f;

	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        Vector3 pos = puck.position;
        Vector3 dir = pos - rb.position;
        Vector3 vel = dir.normalized * speed;
        // check is this speed is going to overshoot the target
        float move = speed * Time.fixedDeltaTime;
        float distToTarget = dir.magnitude;
        if (move > distToTarget)
        {
            // scale the velocity down appropriately
            vel = vel * distToTarget / move;
        }
        rb.velocity = vel;
    }

  
}
